#!/bin/sh

###################
# installazione programmi per il coretto funzionamento di voip.py
###################
sudo apt-get install mpg123 twinkle openvpn -y

###################
# installazione librerie per eseguzione voip.py
##################
sudo pip3 install adafruit-circuitpython-matrixkeypad -y
sudo pip3 install pynput -y